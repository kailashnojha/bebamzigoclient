/* tslint:disable */

declare var Object: any;
export interface VehicleInterface {
  "id"?: any;
  "peopleId"?: any;
  people?: any;
}

export class Vehicle implements VehicleInterface {
  "id": any;
  "peopleId": any;
  people: any;
  constructor(data?: VehicleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Vehicle`.
   */
  public static getModelName() {
    return "Vehicle";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Vehicle for dynamic purposes.
  **/
  public static factory(data: VehicleInterface): Vehicle{
    return new Vehicle(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Vehicle',
      plural: 'Vehicles',
      path: 'Vehicles',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "peopleId": {
          name: 'peopleId',
          type: 'any'
        },
      },
      relations: {
        people: {
          name: 'people',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'peopleId',
          keyTo: 'id'
        },
      }
    }
  }
}
