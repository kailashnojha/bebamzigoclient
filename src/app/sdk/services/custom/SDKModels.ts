/* tslint:disable */
import { Injectable } from '@angular/core';
import { Email } from '../../models/Email';
import { People } from '../../models/People';
import { Transaction } from '../../models/Transaction';
import { Vehicle } from '../../models/Vehicle';
import { Container } from '../../models/Container';
import { VehicleType } from '../../models/VehicleType';
import { Job } from '../../models/Job';
import { JobApply } from '../../models/JobApply';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    Email: Email,
    People: People,
    Transaction: Transaction,
    Vehicle: Vehicle,
    Container: Container,
    VehicleType: VehicleType,
    Job: Job,
    JobApply: JobApply,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
