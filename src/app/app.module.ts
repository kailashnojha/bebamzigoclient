import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SDKBrowserModule } from './sdk/index';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { HomeService } from './services/home/home.service';
import { RoutingModule } from './routing/routing.module';
import { SignupComponent } from './component/signup/signup.component';
import { LoginComponent } from './component/login/login.component';
import { FileDirective } from './directives/file/file.directive';
import { MultipartService } from './services/multipart/multipart.service';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { AddVehicleComponent } from './component/add-vehicle/add-vehicle.component';
import { ProfileComponent } from './component/profile/profile.component';
import { VehicleAssessmentComponent } from './component/vehicle-assessment/vehicle-assessment.component';
import { AddJobComponent } from './component/add-job/add-job.component';
import { DirectionService } from './services/direction/direction.service';
import { JobListingComponent } from './component/job-listing/job-listing.component';
import { ViewJobComponent } from './component/view-job/view-job.component';
import { DateFormatPipePipe } from './pipe/date-format-pipe.pipe';
import { FormatingFunctionsService } from './services/formating-functions.service';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { TestDirectiveDirective } from './directives/test-directive.directive';
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";

import { AngularFireModule } from 'angularfire2';

// New imports to update based on AngularFire2 version 4
//import { AngularFireDatabaseModule } from 'angularfire2/database';
//import { AngularFireAuthModule } from 'angularfire2/auth';
  
export const firebaseConfig = {
	apiKey: "AIzaSyDgEE9ckAPozjnfn-9xupqXvrOwP9SO5t8",
    authDomain: "bebamzigo-44c96.firebaseapp.com",
    databaseURL: "https://bebamzigo-44c96.firebaseio.com",
    projectId: "bebamzigo-44c96",
    storageBucket: "",
    messagingSenderId: "848094809651"
};

let config = new AuthServiceConfig([
	{
		id: FacebookLoginProvider.PROVIDER_ID,
		provider: new FacebookLoginProvider("561602290896109") // //1038388082968751
	}
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SignupComponent,
    LoginComponent,
    FileDirective,
    AddVehicleComponent,
    ProfileComponent,
    VehicleAssessmentComponent,
    AddJobComponent,
    JobListingComponent,
    ViewJobComponent,
    DateFormatPipePipe,
    TestDirectiveDirective,
 
  ],
  imports: [
	BrowserAnimationsModule,
    BrowserModule,
    RoutingModule,
    FormsModule,
	ToastModule.forRoot(),
    SDKBrowserModule.forRoot(),
	AngularFireModule.initializeApp(firebaseConfig),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAKqYlReSqm0QRM8Zi7XjwRQBT6Q85uhDA',
      libraries:["places"]
    }),
	SocialLoginModule,
    AgmDirectionModule  
  ],
  providers: [
	{
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
	FormatingFunctionsService,
    HomeService,
    MultipartService,
    DirectionService,
    GoogleMapsAPIWrapper
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
