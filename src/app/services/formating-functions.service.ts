import { Injectable} from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { LoopBackAuth} from '../sdk/index';

@Injectable()
export class FormatingFunctionsService {

	authCred : any = {};
	constructor(public toastr : ToastsManager, public auth : LoopBackAuth) {
		
	}
  
	formatDate(date){
		let newDate = new Date(date);
		
		let weekDay;
		switch(newDate.getDay()){
			case 0 :
				weekDay = 'Mon';
				break;
			case 1 :
				weekDay = 'Tue';
				break;
			case 2 :
				weekDay = 'Wed';
				break;
			case 3 :
				weekDay = 'Thur';
				break;
			case 4 :
				weekDay = 'Fri';
				break;
			case 5 :
				weekDay = 'Sat';
				break;
			case 6 :
				weekDay = 'Sun';
				break;
		}
		
		return weekDay + ' ' + newDate.getDate() + '. ' + newDate.getMonth() + ' ' + newDate.getFullYear(); 
	}
	
	successToast(mess1,mess2){
		this.toastr.success(mess1,mess2);
	}
	
	errorToast(mess1,mess2){
		this.toastr.error(mess1,mess2);
	}
	
	setAuthCredential(){
		this.authCred.tokenId = this.auth.getAccessTokenId();
		this.authCred.role = (this.auth.getCurrentUserData() || {}).realm;
		return this.authCred;
	}
}
