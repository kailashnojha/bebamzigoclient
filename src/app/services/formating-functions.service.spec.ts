import { TestBed, inject } from '@angular/core/testing';

import { FormatingFunctionsService } from './formating-functions.service';

describe('FormatingFunctionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormatingFunctionsService]
    });
  });

  it('should be created', inject([FormatingFunctionsService], (service: FormatingFunctionsService) => {
    expect(service).toBeTruthy();
  }));
});
