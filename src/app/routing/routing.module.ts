import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './../component/home/home.component';
import { SignupComponent } from './../component/signup/signup.component';
import { LoginComponent } from './../component/login/login.component';
import { AddVehicleComponent } from './../component/add-vehicle/add-vehicle.component';
import { ProfileComponent } from './../component/profile/profile.component';
import { VehicleAssessmentComponent } from './../component/vehicle-assessment/vehicle-assessment.component';
import { AddJobComponent } from './../component/add-job/add-job.component';
import { JobListingComponent } from './../component/job-listing/job-listing.component';

const routes: Routes = [
	{ path: '', redirectTo: '/', pathMatch: 'full' },
	{ path: '', component: HomeComponent },
	{ path: 'signup', component: SignupComponent },
	{ path: 'login', component: LoginComponent },   
	{ path: 'add-vehicle', component: AddVehicleComponent },
	{ path: 'profile', component: ProfileComponent },
	{ path: 'vehicle-assessment/:vehicleId', component: VehicleAssessmentComponent },
	{ path: 'job/:mode/:type', component: AddJobComponent },
	{ path: 'job-listing', component : JobListingComponent },
	{ path: 'job-listing/:active', component : JobListingComponent } 	
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class RoutingModule { }
