import { Directive,HostListener } from '@angular/core';

@Directive({
  selector: '[appTestDirective]'
})
export class TestDirectiveDirective {

	constructor() { }
  
	@HostListener("keypress", ["$event"])
	onKeyPress(e: KeyboardEvent): boolean {
		const pattern_price = /^\d{0,8}(\.\d{1,4})?$/;
		if(e.keyCode != 8 && !pattern_price.test(e.key)){
			return false;
		}else{
			return true;
		}
	}
}
