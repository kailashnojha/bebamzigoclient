import { Component, OnInit, Renderer ,ViewContainerRef  } from '@angular/core';
import { LoopBackConfig, LoopBackAuth, PeopleApi,JobApi } from './sdk/index';
import { Router, NavigationEnd } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  authCred : any = {};
  hideHeaderOn:Array<string> = ["/job/add/on-demand","/job/add/standard-rate"]
  isHeaderHide = false;
  isAppliedJobs : boolean = false;
  isActiveJobs : boolean = false;
  constructor(private jobApi : JobApi,public auth: LoopBackAuth,private router: Router, private peopleApi:PeopleApi,public toastr : ToastsManager, private vcr: ViewContainerRef){
  	LoopBackConfig.setBaseURL('http://139.59.71.150:3000');
    LoopBackConfig.setApiVersion('api');
	this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
  	if($('.preloader').length){
    		$('.preloader').delay(200).fadeOut(500);
  	}	
    this.setAuthCredential();
	this.getAppliedJobs();
    this.router.events.subscribe((evt) => {
		if (!(evt instanceof NavigationEnd)) {
			return;
		}
		if(this.hideHeaderOn.indexOf(evt.url) != -1){
			this.isHeaderHide = true;
		}
		if(this.hideHeaderOn.indexOf(evt.url) == -1){
			this.isHeaderHide = false;
		}
		if(evt.url === '/' && this.authCred.tokenId){
			this.router.navigate(['profile']);
		}
	});
		
	var scrollToTop = window.setInterval(function () {
		var pos = window.pageYOffset;
		if (pos > 0) {
			window.scrollTo(0, pos - 20); // how far to scroll on each step
		} else {
			window.clearInterval(scrollToTop);
		}
	}, 0); // how fast to scroll (this equals roughly 60 fps)
  }

  setAuthCredential(){
    this.authCred.tokenId = this.auth.getAccessTokenId();
    this.authCred.role = (this.auth.getCurrentUserData() || {}).realm;
	}
  
	getAppliedJobs(){
	  let _self = this;
		this.jobApi.getJobList('driver').subscribe(
			(success) => {
				console.log("success : ", success);
				if( success.success.data.length > 0){
					_self.isAppliedJobs = true;
					_self.isActiveJobs = true;
				}
			},
			(error)=>{
				console.log("error : ", error);
			}
		);
	}

	login(){
		if(this.authCred.tokenId){
		  
		}
		this.router.navigate(['login']);
	}
  
	signup(){
		this.router.navigate(['signup']);
	}

	logout(){
		this.peopleApi.logout().subscribe(()=>{
			this.setAuthCredential();  
			this.router.navigate(['']);
		},()=>{
		});
	}
}
