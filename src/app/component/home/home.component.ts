import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../services/home/home.service';
// import * as jquery from "jquery";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private homeService:HomeService) { }

  ngOnInit() {
	this.homeService.initNewsSlide();
	this.homeService.initSingleSlide();
	this.homeService.initSlider1();
	this.homeService.initSlidePartner();
  }

}
