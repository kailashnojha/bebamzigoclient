import { Component, OnInit, NgZone, AfterViewInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import { MapsAPILoader, GoogleMapsAPIWrapper} from '@agm/core';
import {} from '@types/googlemaps';
import { VehicleTypeApi,JobApi,LoopBackAuth, LoopBackConfig } from './../../sdk/index';
import { DirectionService } from './../../services/direction/direction.service';
import { MultipartService } from './../../services/multipart/multipart.service';
import {Http, Headers,RequestOptions} from '@angular/http';
import {FormatingFunctionsService} from './../../services/formating-functions.service';

@Component({
  selector: 'app-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: ['./add-job.component.css']
})
export class AddJobComponent implements OnInit {
  type:string;
  mode:string;
  jobObj:any = {};	
  vehicleTypes:any;
  machineryTypes : any;
  selectedVehicle : any;
  latitude: number;
  longitude: number;
  zoom: number;
  
  routes:any = [];
  selectedRoute:any = [];
  dir:any;
  request:any = {};
  phase1 : boolean;
  phase2 : boolean;
  phase3 : boolean;
  scheduledDate : any;
  scheduledTime : any;
  bounds : any;
  showRoutes : boolean = false;
  selectedRoutes : any;
  
  baseUrl : String;
  errArr : any = {};
  
  constructor(private router : Router, public formatingFunctionsService : FormatingFunctionsService,public gMaps: GoogleMapsAPIWrapper,public auth : LoopBackAuth, private http : Http, private multipartService : MultipartService,private jobApi : JobApi,private directionService:DirectionService,private vehicleTypeApi:VehicleTypeApi,private route: ActivatedRoute,private mapsAPILoader:MapsAPILoader, private ngZone:NgZone) {
  	this.route.params.subscribe( params => {
  		this.type = params.type;
  		this.mode = params.mode;
  		this.jobObj.typeOfJob = this.type == 'standard-rate' ? 'standard rate' : 'on demand'; 
		this.baseUrl = LoopBackConfig.getPath();
  	});
  }

	ngOnInit() {
		
		this.phase1 = true;
		this.getVehicleType();
		this.zoom = 4;
		this.latitude = 39.8282;
		this.longitude = -98.5795;
		this.setCurrentPosition();
	}
  
	ngAfterViewInit() {
		let _self = this;
		this.initializeDatepicker();
		this.initializeClockpicker();
		this.initializeMap();
		$('#date').datepicker().on('changeDate', function (ev) {
			console.log("data : ", ev);
			let currentDate = new Date();
			let time = _self.currentTime();
			_self.scheduledDate = ev.date;
			if(this.errArr && this.errArr.day){
				this.errArr.day = false;
			}
			if(typeof _self.scheduledTime !== 'undefined' && _self.scheduledTime < time && ((_self.scheduledDate.getDate() === currentDate.getDate() && _self.scheduledDate.getMonth() === currentDate.getMonth() && _self.scheduledDate.getFullYear() === currentDate.getFullYear() )) ){
				_self.jobObj.time = '';
			}
		});
		
		$('#time').clockpicker().change(function(event){
			console.log("clock value : ",this.value);
			let currentDate = new Date();
			let time = _self.currentTime();
			_self.scheduledTime = this.value;
			if(this.errArr && this.errArr.time){
				this.errArr.time = false;
			}
			if((time > this.value) && _self.scheduledDate !== undefined && (_self.scheduledDate.getDate() === currentDate.getDate() && _self.scheduledDate.getMonth() === currentDate.getMonth() && _self.scheduledDate.getFullYear() === currentDate.getFullYear() )){
				event.stopPropagation();
				$('#time').clockpicker('hide');
				console.log("true : ",time);
				$('#time')[0].value = '';
			}
		});
	}
	
	initializeDatepicker(){
		var date_input = $('input[name="date"]'); //our date input has the name "date"
		let _self = this;
	
		date_input.datepicker({
			format: 'D d. M yyyy',
			todayHighlight: true,
			autoclose: true,
			numberOfMonths: 1,
			defaultDate: "+1d",
			startDate: '+0d'
		});
	}
	
	initializeClockpicker(){
		$('#time').clockpicker({
			'default': 'now',       // default time, 'now' or '13:14' e.g.
			fromnow: 0,          // set default time to * milliseconds from now (using with default = 'now')
			placement: 'bottom', // clock popover placement
			align: 'left',       // popover arrow align
			donetext: 'Done',    // done button text
			autoclose: true,    // auto close when minute is selected
			twelvehour: false, // change to 12 hour AM/PM clock from 24 hour
			vibrate: false
		});
	}
	
	initializeMap(){
		this.mapsAPILoader.load().then(()=>{
			let options = {
				types: ['(cities)'],
				componentRestrictions: {country: 'ke'}
			};//,{types:["address"]}
			let pickupAutocomplete = new google.maps.places.Autocomplete(<HTMLInputElement>document.getElementById("pickupAutocomplete"),options);
			pickupAutocomplete.addListener('place_changed', () => {
				this.jobObj.source = {};
				let place: google.maps.places.PlaceResult = pickupAutocomplete.getPlace();
				this.pickupLocation(place);
			});

			let dropAutocomplete = new google.maps.places.Autocomplete(<HTMLInputElement>document.getElementById("dropAutocomplete"),options);
			dropAutocomplete.addListener('place_changed', () => {
				this.jobObj.destination = {};
				let place: google.maps.places.PlaceResult = dropAutocomplete.getPlace();
				this.dropLocation(place);
			});
		});
	}

	currentTime(){
		let currentDate = new Date();
		let minutes;
		let hour;
		let time;
		if(0 < currentDate.getMinutes() && currentDate.getMinutes() < 10){
			minutes = '0' + currentDate.getMinutes();
		}
		if(currentDate.getMinutes() >= 10){
			minutes = currentDate.getMinutes();
		}
		if(0 < currentDate.getHours() && currentDate.getHours() < 10){
			hour = '0' + currentDate.getHours();
		}
		if(currentDate.getHours() >= 10){
			hour = currentDate.getHours();
		}
		
		time = hour+':'+minutes; 
		
		return time;
	}
	
	setCurrentPosition() {
		if ("geolocation" in navigator) {
			navigator.geolocation.getCurrentPosition((position) => {
				//console.log("position : ", position);
				this.latitude = position.coords.latitude;
				this.longitude = position.coords.longitude;
				this.zoom = 12;
			});
		}
	}
	
	backPhase2(){
		this.phase1=true;
		this.phase2=false;
		this.phase3=false;
		this.setCurrentPosition();
		
		setTimeout(()=>{ 
			this.ngAfterViewInit();
			$('#pickupAutocomplete')[0].value = this.jobObj.source.address;
			$('#dropAutocomplete')[0].value = this.jobObj.destination.address;
			$('#date')[0].value = this.formatingFunctionsService.formatDate(this.scheduledDate);
			$('#time')[0].value = this.scheduledTime;
			this.errArr.source = false;
			this.errArr.destination = false;
			this.errArr.day = false;
			this.errArr.time = false;
		},500);
	}
	
	setPosition() {
		this.latitude = this.jobObj.source.location.lat;
		this.longitude = this.jobObj.source.location.lng;
		
		let Item_1 = new google.maps.LatLng(this.jobObj.source.location.lat,this.jobObj.source.location.lng);

		let myPlace = new google.maps.LatLng(this.jobObj.destination.location.lat,this.jobObj.destination.location.lng);
		
		this.bounds = new google.maps.LatLngBounds();
		this.bounds.extend(Item_1);
		this.bounds.extend(myPlace);
	}
	
	getDirection(source:any,destination:any){
		let _self = this;
		this.directionService.getDirection(source,destination,(response: any, status: any) => {
			if (status === 'OK') {
				_self.routes = response.routes;
				console.log("routes : ",_self.routes);
				_self.setPosition();
			}else{
				_self.formatingFunctionsService.errorToast('No route found !!!','oops');
				console.log("come in error part : " , response);
			}
		})
	}
	
	

	getSelectedRoute(route:any){

	}

	getVehicleType() {
		let _self = this;
		this.vehicleTypeApi.getVehicleType().subscribe((success)=>{
			let vehicleArr = success.success.data.vehicle;
			let machineryArr = success.success.data.machinery;
			
			for(var i=0;i<=vehicleArr.length-1;i++){
				vehicleArr[i].imgUrl = _self.baseUrl + vehicleArr[i].imgUrl;
			}
			
			for(var i=0;i<=machineryArr.length-1;i++){
				machineryArr[i].imgUrl = _self.baseUrl + machineryArr[i].imgUrl;
			}
			
			this.vehicleTypes = vehicleArr;
			this.machineryTypes = machineryArr;
  		
		},(error)=>{
			console.log(error)
		})	
	}

	pickupLocation(place){
		this.jobObj.source = {
			address:place.formatted_address,
			location:{
				lat:place.geometry.location.lat(),
				lng:place.geometry.location.lng()
			}
		}
		
		if(this.errArr.source){
			this.errArr.source = false;
		}
	}

	dropLocation(place){
		this.jobObj.destination = {
			address:place.formatted_address,
			location:{
				lat:place.geometry.location.lat(),
				lng:place.geometry.location.lng()
			}
		}
		
		if(this.errArr.drop){
			this.errArr.drop = false;
		}
	}

	selectVehicle(vehicle:any){
	
		this.jobObj.typeOfVehicle = vehicle.name;
		this.selectedVehicle = vehicle;
	
		if(this.errArr.vehicle){	
			this.errArr.vehicle = false;
		}
	}
  
	selectRoute(route){
		this.selectedRoutes = route;
		this.selectedRoute = [];
		for(let i=0;i<= route.overview_path.length-1;i++){
			this.selectedRoute.push({lat : route.overview_path[i].lat(),lng : route.overview_path[i].lng()});
		}
		
		this.jobObj.selectedRoute = this.selectedRoute;
		this.jobObj.distance = route.legs[0].distance;
		this.jobObj.duration = route.legs[0].duration;
	}
  
	submitPhase1(){
	
		if(this.jobObj.source && this.jobObj.destination && this.selectedVehicle && $('#time')[0].value && $('#date')[0].value){
			this.jobObj.date = new Date( $('#date')[0].value + ' ' + $('#time')[0].value );
			this.getDirection(this.jobObj.source.location,this.jobObj.destination.location);
			this.phase1 = false;
			this.phase2 = true;
			this.phase3 = false; 
		}
		else{
			if(!this.jobObj.source){
				this.errArr.source = true;
			}
			else{
				this.errArr.source = false;
			}
			if(!this.jobObj.destination){
				this.errArr.drop = true;
			}
			else{
				this.errArr.drop = false;
			}
			if(!this.selectedVehicle  ){
				this.errArr.vehicle = true;
			}
			else{
				this.errArr.vehicle = false;
			}
			if(!$('#date')[0].value){
				this.errArr.day = true;
			}
			else{
				this.errArr.day = false;
			}
			if(!$('#time')[0].value){
				this.errArr.time = true;
			}
			else{
				this.errArr.time = false;
			}
		}
	}
  
	submitPhase2(){
		if(this.selectedVehicle.ton.length > 0){
			if(this.jobObj.typeOfVehicle && this.jobObj.distance && this.jobObj.duration && this.jobObj.title && this.jobObj.description && this.jobObj.vehicleTonnage){
				this.getPrice();
			}
			else{
				this.validatePhase2();
				if(!this.jobObj.vehicleTonnage){
					this.errArr.vehicleTonnage = true;
				}
				else{
					this.errArr.vehicleTonnage = false;
				}
			}
		}
		if(this.selectedVehicle.bodyType.length > 0){
			if(this.jobObj.typeOfVehicle && this.jobObj.distance && this.jobObj.duration && this.jobObj.title && this.jobObj.description && this.jobObj.vehicleBodyType){
				this.getPrice();
			}
			else{
				this.validatePhase2();
				if(!this.jobObj.vehicleBodyType){
					this.errArr.vehicleBodyType = true;
				}
				else{
					this.errArr.vehicleBodyType = false;
				}
			}
		}
	}
  
	getPrice(){
		let _self = this;
		if(this.jobObj.vehicleTonnage === undefined){
			this.jobObj.vehicleTonnage = 0;
		}
		if(this.jobObj.typeOfJob === 'on demand'){
			this.jobApi.getJobPricing(this.jobObj.typeOfVehicle, this.jobObj.vehicleTonnage ,this.jobObj.distance.value).subscribe(
				(success)=>{
					console.log("success : ", success);
					_self.jobObj.netPrice  = success.success.data;
					_self.switchPhase2();
				},
				(error) => {
					_self.formatingFunctionsService.errorToast('Cannot get the pricing, please try again !!!','oops');
					console.log("error : ",error);
				}
			);
		}
		else{
			this.switchPhase2();
		}
	}
  
	switchPhase2(){
		this.phase1 = false;
		this.phase2 = false;
		this.phase3 = true;
	}
  
	validatePhase2(){
		if(this.selectedRoute.length == 0){
			this.errArr.route = true;
		}
		else{
			this.errArr.route = false;
		}
		if(!this.jobObj.title){
			this.errArr.title = true;
		}
		else{
			this.errArr.title = false;
		}
		if(!this.jobObj.description){
			this.errArr.description = true;
		}
		else{
			this.errArr.description = false;
		}
	}
	
	validatePhase3(){
		
		let error = 0;
		if(this.jobObj.typeOfJob === 'standard rate'){
			if(this.jobObj.budget){
				this.errArr.budget = false;
			}
			else{
				this.errArr.budget = true;
				error = 1;
			}
		}
		else{
			
		}
		if(this.jobObj.natureOfGoods){
			this.errArr.natureOfGoods = false;
		}
		else{
			this.errArr.natureOfGoods = true;
			error = 1;
		}
		if(this.jobObj.valueOfGoods){
			this.errArr.valueOfGoods = false;
		}
		else{
			this.errArr.valueOfGoods = true;
			error = 1;
		}
		if(this.jobObj.weight){
			this.errArr.weight = false;
			if(this.jobObj.weight > this.jobObj.vehicleTonnage){
				this.errArr.weightMore = true;
				error = 1;
			}
			else{
				this.errArr.weightMore = false;
			}
		}
		else{
			this.errArr.weight = true;
			error = 1;
		}
		if(error == 1){
			return false;
		}
		else{
			return true;
		}
	}
  
	createJob(){
		let _self = this;
		if(this.validatePhase3()){
			//this.jobObj.date = new Date();
	  
			if(this.jobObj.budget === undefined){
				this.jobObj.budget = 0;
			}
			if(this.jobObj.netPrice === undefined){
				this.jobObj.netPrice = 0;
			}
			if(this.jobObj.vehicleTonnage === undefined){
				this.jobObj.vehicleTonnage = 0;
			}
	  
			this.jobApi.createJob({},this.jobObj.title,this.jobObj.weight,
				this.jobObj.source,this.jobObj.destination,
				+this.jobObj.valueOfGoods,+this.jobObj.budget,
				+this.jobObj.netPrice,this.jobObj.typeOfVehicle,
				this.jobObj.date, this.jobObj.description,
				this.jobObj.natureOfGoods, this.jobObj.typeOfJob,
				this.jobObj.selectedRoute,
				this.jobObj.distance,this.jobObj.duration,+this.jobObj.vehicleTonnage,
				this.jobObj.vehicleBodyType
			).subscribe((success)=>{
				console.log("success : ",success);
				_self.formatingFunctionsService.successToast('Job added successfully !!!','Job Created');
				_self.router.navigate(['job-listing']);
			},(error)=>{
				console.log("error : ",error);
			});
		}
	}
}
