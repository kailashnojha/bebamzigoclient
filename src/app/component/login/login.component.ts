import { Component, OnInit } from '@angular/core';
import { PeopleApi } from './../../sdk/index';
import { Router } from '@angular/router';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider } from 'angularx-social-login';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	user:any = {};	
	errSuccObj :any = {};
	
	constructor(public fAuth: AngularFireAuth, private peopleApi:PeopleApi, private router:Router,private authService: AuthService) {
		
	}

	ngOnInit() {
		// this.authService.authState.subscribe((user) => {
			// this.user = user;
		// });
		//this.signOut();
	}
  
	signInWithFB(): void {
		this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(
			(success) => {
				console.log("success : ", success.authToken);
			}).
			catch(
				(err) => {
					console.log("error : ", err);
				});
		//console.log("facebook : ", firebase.auth());
		// this.fAuth.auth.signInWithPopup(new firebase.auth.FacebookLoginProvider()).then(
			// (success) => {
				// console.log("success : ", success);
			// }).
			// catch(
				// (err) => {
					// console.log("error : ", err);
				// });
	}
  
	signOut(): void {
		this.authService.signOut();
	}

	login(loginForm:any){
		if(loginForm.valid){
			var $btn = $('#loginBtn').button('loading');
			this.user.rememberMe = true;
			console.log("*************login****************");
			console.log(this.user);
			console.log("*************login****************");
			this.peopleApi.login(this.user,'user',this.user.rememberMe).subscribe((success)=>{
				this.router.navigate(['/profile']);
				$btn.button('reset');    
			},(error)=>{
				$btn.button('reset');
				console.log(error);
				this.errSuccObj.error = error.message;
			})    
		}
	}
}
