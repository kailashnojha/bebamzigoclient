import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleAssessmentComponent } from './vehicle-assessment.component';

describe('VehicleAssessmentComponent', () => {
  let component: VehicleAssessmentComponent;
  let fixture: ComponentFixture<VehicleAssessmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleAssessmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
