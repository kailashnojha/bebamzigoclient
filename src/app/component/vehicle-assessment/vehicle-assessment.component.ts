import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {VehicleApi} from "./../../sdk/index";

@Component({
  selector: 'app-vehicle-assessment',
  templateUrl: './vehicle-assessment.component.html',
  styleUrls: ['./vehicle-assessment.component.css']
})
export class VehicleAssessmentComponent implements OnInit {
  vehicleId : any;	
  vehicle : any;
  constructor(private route: ActivatedRoute,private vehicleApi:VehicleApi) { 
  	this.route.params.subscribe( params => {
  		this.vehicleId = params.vehicleId;
  	});
  }

  ngOnInit() {
  	this.getVehicleInfo();
  }

  getVehicleInfo(){
  	this.vehicleApi.getVehicleById(this.vehicleId).subscribe((vehicle)=>{
  		this.vehicle = vehicle.success.data;
  	},(error)=>{

  	})
  }



}
