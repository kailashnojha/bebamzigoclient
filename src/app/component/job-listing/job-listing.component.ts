import { Component, OnInit, NgZone } from '@angular/core';
import { JobApi,LoopBackAuth } from './../../sdk/index';
import { DatePipe,CurrencyPipe  } from '@angular/common';
import { MapsAPILoader} from '@agm/core';
import {} from '@types/googlemaps';
import {FormatingFunctionsService} from './../../services/formating-functions.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-job-listing',
  templateUrl: './job-listing.component.html',
  styleUrls: ['./job-listing.component.css']
})
export class JobListingComponent implements OnInit {

	jobList : any = [];
	showList : boolean = true;
	selectedJob : any;
	bounds : any;
	selectedRoute : any;
	role : any;
	active : any;
	realm : any;
	constructor(private route : ActivatedRoute,private jobApi:JobApi, private mapsAPILoader : MapsAPILoader, private ngZone:NgZone, private formatingFunctionsService : FormatingFunctionsService) { }

	ngOnInit() {
		this.route.params.subscribe( params => {
			this.active = params.active;
			console.log("active : ",this.active);
			if(this.active === undefined){
				this.realm = 'customer';
			}
			if(this.active === 'active'){
				this.realm = 'driver';
			}
		});
		
		this.getJobList(); 
		this.mapsAPILoader.load().then(()=>{
			console.log("map loaded");
		});
		this.role = this.formatingFunctionsService.setAuthCredential().role;
		console.log("role : ", this.role);
	}

	getJobList(){
		let _self = this;
		let arr = [];
		this.jobApi.getJobList(this.realm).subscribe(
			(success) => {
				console.log("success : ", success);
				_self.jobList = success.success.data;
			},
			(error)=>{
				console.log("error : ", error);
			}
		);	
	}
	
	Cancel(job){
		
	}
	
	applyForJob(job){
		let _self = this;
		this.jobApi.applyForJob({},job.id).subscribe((success)=>{
			console.log(success);
			_self.formatingFunctionsService.successToast("Congratulations, Job Applied successfully !!!","Job Applied");
		},(error)=>{
			_self.formatingFunctionsService.errorToast(error.message,"Oops");
			console.log(error);
		});
	}
	
	viewJob(job){
		this.showList = false;
		this.selectedJob = job;
		this.selectedRoute = this.selectedJob.selectedRoute;
		let Item_1 = new google.maps.LatLng(this.selectedJob.source.location.lat,this.selectedJob.source.location.lng);

		let myPlace = new google.maps.LatLng(this.selectedJob.destination.location.lat,this.selectedJob.destination.location.lng);
		
		this.bounds = new google.maps.LatLngBounds();
		this.bounds.extend(Item_1);
		this.bounds.extend(myPlace);
	}
}
