import { Component, OnInit } from '@angular/core';
import { PeopleApi, VehicleApi } from './../../sdk/index'; 
import { MultipartService } from './../../services/multipart/multipart.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit { 
  
  user:any={};
  tabValue:string="password"; 	 
  changePass: any = {};
  isPassFormSubmit: boolean = false;
  changePassObj:any={};
  errSuccObj :any = {};
  errSuccDoc :any = {};
  vehicles:any = [];
  editUser:any = {};
  docExtra:any = {};
  docFiles:any = {};
  constructor(private peopleApi:PeopleApi, private vehicleApi: VehicleApi,private multipartService:MultipartService) { }
  
  ngOnInit() {
  	this.getUserInfo();
    this.getMyTruck();
  }

  getUserInfo(){
  	this.peopleApi.getMyInfo().subscribe((userIns:any)=>{
  		this.user = userIns.success.data;
      // this.editUser = userIns.success;
  		console.log(userIns);
  	},(err:any)=>{

  	})
  }

  editProfile(editForm:any){
    if(editForm.valid){
      var $btn = $('#editProfileBtn').button('loading');
      this.peopleApi.editProfile(this.editUser.name,this.editUser.mobile,this.editUser.email).subscribe((userInst)=>{
        this.errSuccObj.error = undefined;
        this.errSuccObj.success = "Profile update successfully";
        $btn.button('reset'); 
      },(error)=>{
        this.errSuccObj.error = error.message;
        this.errSuccObj.success = undefined;
        $btn.button('reset'); 
      })
    }  
  }  

  uploadDocument(){
    
     var $btn = $('#uploadDocBtn').button('loading');
     this.multipartService.uploadDriverDoc({files:this.docFiles}).subscribe((userInst)=>{
        console.log(userInst);
        this.errSuccDoc.error = undefined;
        this.errSuccDoc.success = "Upload document successfully";
        $btn.button('reset'); 
     },(error)=>{
       this.errSuccDoc.error = error.message;
       this.errSuccDoc.success = undefined;
       $btn.button('reset'); 
     }) 
  }

  changePassword(changePassForm){
  	this.isPassFormSubmit = true;
  	  if(!changePassForm.valid) return;
    	if(this.changePass.newPassword == this.changePass.confirmPassword){
        var $btn = $('#changePassBtn').button('loading');
  	  	this.peopleApi.changePassword(this.changePass.currentPassword,this.changePass.newPassword).subscribe((success)=>{
  	  		this.changePassObj.error = undefined;
  	  		this.changePassObj.success = "Change password successfully";
          $btn.button('reset'); 
  	  	},(error)=>{
  	  		this.changePassObj.error = error.message;
  	  		this.changePassObj.success = undefined;
          $btn.button('reset'); 
  	  		// console.log(error);
  	  	})	  		
  	}else{
        this.changePassObj.error = "confirm password does not match";
        this.changePassObj.success = undefined;
  	}
  }

  getMyTruck(){
    this.vehicleApi.getMyVehicles().subscribe((vehiclesInst)=>{
       this.vehicles = vehiclesInst.success.data;
    },(error)=>{

    })
  }

  selectTab(tabValue:string){
  	this.tabValue = tabValue;
    if(this.tabValue == 'profile') this.editUser = Object.assign({},this.user); 
  }

}
