import { Component, OnInit ,NgZone} from '@angular/core';
import { MultipartService } from '../../services/multipart/multipart.service';
import { MapsAPILoader} from '@agm/core';
import {} from '@types/googlemaps';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  files:any = {};
  user:any = {};  
  errSuccObj :any = {};
  constructor(private multipartApi:MultipartService,private mapsAPILoader:MapsAPILoader, private ngZone:NgZone) { 
      this.user.realm = "driver";
      this.user.extra = {};
      this.user.address = {};
  }

  ngOnInit() {
      /*this.mapsAPILoader.load().then(()=>{
        let autocomplete = new google.maps.places.Autocomplete(<HTMLInputElement>document.getElementById("addressAutocomplete"),{types:["address"]})
        autocomplete.addListener('place_changed', () => {
              this.ngZone.run(() => {
                let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                this.user.address.location = {
                  lat : place.geometry.location.lat(),
                  lng : place.geometry.location.lng()  
                }
                this.user.address.address =  place.formatted_address;
              });
          });
      });*/
  }

  signup(signupForm){
    
    if(signupForm.valid){
      if(this.user.password == this.user.confirmPassword){
        var $btn = $('#signupBtn').button('loading');
        let data = Object.assign({}, this.user);
        this.multipartApi.signupApi({data:data,files:this.files}).subscribe((success:any)=>{
          this.errSuccObj.error = undefined;
          this.errSuccObj.success = "Signup successfully";
          $btn.button('reset'); 
        },(error:any)=>{
          this.errSuccObj.error = error.message;
          this.errSuccObj.success = undefined;
          $btn.button('reset'); 
        })
      }else{
        this.errSuccObj.error = "confirm password does not match";
        this.errSuccObj.success = undefined;
      }  
    }    
  }


}
