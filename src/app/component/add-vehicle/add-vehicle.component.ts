import { Component, OnInit } from '@angular/core';
import { VehicleTypeApi } from "../../sdk/index";
import { MultipartService } from '../../services/multipart/multipart.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-vehicle',
  templateUrl: './add-vehicle.component.html',
  styleUrls: ['./add-vehicle.component.css']
})
export class AddVehicleComponent implements OnInit {
  vehicleTypes : any = {};	
  selectedMachineType: any;
  selectTypeValue:string;
  type : any = {};
  vehicle:any;
  files:any = {};
  errSuccObj :any = {};
  constructor(private vehicleTypeApi:VehicleTypeApi, private multipartApi:MultipartService, private router:Router) {
  	this.vehicle = this.defaultVehicle();	
  }

  defaultVehicle(){
  	return {
	  	type:"",
	  	ton:"",
	  	bodyType:"",
	  	extra:{}
  	};
  }

  ngOnInit() {
  	this.getVehicleType();
  }

  machineSelect(type:string){
  	if(type == this.selectTypeValue)
  		return;
  	this.vehicle = this.defaultVehicle();
  	this.type = {};
  	this.selectedMachineType = this.vehicleTypes[type];
  	if(this.selectedMachineType)
  		this.selectTypeValue = type;
  	// console.log(this.selectedMachineType);

  }

  typeSelect(typeStr:string){
  	if(typeStr == ""){
  		this.type = {};
  	}else{
  		this.selectedMachineType.forEach((value)=>{
  			if(value.name == typeStr){
  				this.type = value;
  			}
  		})
  	}

  }

  addVehicle(addVehicleForm:any){
    if(addVehicleForm.valid){
      var $btn = $('#addVehicleBtn').button('loading');
      let data= Object.assign({}, this.vehicle);
      this.multipartApi.addVehicle({data:data,files:this.files}).subscribe((success)=>{
        this.router.navigate(['/vehicle-assessment',success.success.data.id]);
        this.errSuccObj.error = undefined;
        // this.errSuccObj.success = "Signup successfully";
        $btn.button('reset'); 
      },(error)=>{
        this.errSuccObj.error = error.message;
        this.errSuccObj.success = undefined;
        $btn.button('reset');
      })  
    }
    	
  }

  getVehicleType(){
  	this.vehicleTypeApi.getVehicleType().subscribe((success)=>{
  		this.vehicleTypes = success.success.data;
  		console.log(this.vehicleTypes);
  	},(err)=>{
      
  	})
  }

}
